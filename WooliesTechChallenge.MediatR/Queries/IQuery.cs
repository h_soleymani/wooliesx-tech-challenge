﻿using MediatR;

namespace WooliesTechChallenge.MediatR.Queries
{
    public interface IQuery<out TResponse> : IRequest<TResponse>
    {
    }
}
