﻿using System.Threading.Tasks;

namespace WooliesTechChallenge.MediatR.Queries
{
    public interface IQueryBus
    {
        Task<TResponse> Send<TQuery, TResponse>(TQuery query) where TQuery : IQuery<TResponse>;
    }
}
