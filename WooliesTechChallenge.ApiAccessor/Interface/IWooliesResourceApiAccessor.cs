﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WooliesTechChallenge.Domain.Entities;

namespace WooliesTechChallenge.ApiAccessor.Interface
{
    public interface IWooliesResourceApiAccessor
    {
        Task<IEnumerable<Product>> GetProducts();

        Task<IEnumerable<ShopperHistory>> GetShopperHistory();
        
        Task<decimal> GetTrolleyTotal(Trolley trolleyTotalRequest);
    }
}
