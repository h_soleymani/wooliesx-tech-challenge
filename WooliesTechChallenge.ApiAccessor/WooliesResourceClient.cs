﻿using System.Net.Http;

namespace WooliesTechChallenge.ApiAccessor
{
    public class WooliesResourceClient
    {
        public HttpClient HttpClient { get; }

        public WooliesResourceClient(HttpClient httpClient)
        {
            HttpClient = httpClient;
        }
    }
}
