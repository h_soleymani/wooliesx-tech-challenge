﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WooliesTechChallenge.ApiAccessor.Interface;
using WooliesTechChallenge.Domain.Entities;

namespace WooliesTechChallenge.ApiAccessor
{
    public class WooliesResourceApiAccessor : IWooliesResourceApiAccessor
    {
        private readonly IConfiguration _config;
        private readonly WooliesResourceClient _client;

        public WooliesResourceApiAccessor(WooliesResourceClient client, IConfiguration config)
        {
            _client = client;
            _config = config;
        }

        public async Task<IEnumerable<Product>> GetProducts()
        {
            var response = await _client.HttpClient
                .GetStringAsync($"{_config["WolliesResource:ApiUrl:Products"]}?token={_config["WolliesResource:Identifier:Token"]}");

            var result = JsonConvert.DeserializeObject<IEnumerable<Product>>(response);

            return result;
        }

        public async Task<IEnumerable<ShopperHistory>> GetShopperHistory()
        {
            var response = await _client.HttpClient
                .GetStringAsync($"{_config["WolliesResource:ApiUrl:ShopperHistory"]}?token={_config["WolliesResource:Identifier:Token"]}");

            var result = JsonConvert.DeserializeObject<IEnumerable<ShopperHistory>>(response);

            return result;
        }

        public async Task<decimal> GetTrolleyTotal(Trolley trolleyTotalRequest)
        {
            var jsonRequest = JsonConvert.SerializeObject(trolleyTotalRequest);
            var reqeustBody = new StringContent(jsonRequest.ToString(), Encoding.UTF8, "application/json");

            var response = await _client.HttpClient
                .PostAsync($"{_config["WolliesResource:ApiUrl:TrolleyCalculator"]}?token={_config["WolliesResource:Identifier:Token"]}", reqeustBody);

            var result = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<decimal>(result);
        }
    }
}
