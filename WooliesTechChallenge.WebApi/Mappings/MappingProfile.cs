﻿using AutoMapper;
using System;
using WooliesTechChallenge.Core.Queries;
using WooliesTechChallenge.Domain.Constants;
using WooliesTechChallenge.Domain.Entities;
using WooliesTechChallenge.WebApi.Models;

namespace WooliesTechChallenge.WebApi.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<SortOption, GetSortedProductsQuery>()
                .ForMember(dest => dest.SortOption, op => op.MapFrom(src => src));

            CreateMap<TrolleyTotalRequest, GetTrolleyTotalQuery>();
            CreateMap<GetTrolleyTotalQuery, Trolley>();
        }
    }
}
