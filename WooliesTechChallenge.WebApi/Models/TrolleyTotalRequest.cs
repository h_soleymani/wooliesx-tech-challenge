﻿using System.Collections.Generic;
using WooliesTechChallenge.Domain.Entities;

namespace WooliesTechChallenge.WebApi.Models
{
    public class TrolleyTotalRequest
    {
        public IEnumerable<Product> Products { get; set; }

        public IEnumerable<Special> Specials { get; set; }

        public IEnumerable<Quantity> Quantities { get; set; }
    }
}
