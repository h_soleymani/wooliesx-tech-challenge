﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WooliesTechChallenge.Core.Queries;
using WooliesTechChallenge.Domain.Constants;
using WooliesTechChallenge.Domain.Entities;
using WooliesTechChallenge.MediatR.Queries;
using WooliesTechChallenge.WebApi.Models;

namespace WooliesTechChallenge.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnswersController : Controller
    {
        private readonly IQueryBus _queryBus;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public AnswersController(IQueryBus queryBus, IMapper mapper, ILogger<AnswersController> logger)
        {
            _queryBus = queryBus ?? throw new ArgumentNullException(nameof(queryBus));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet("user")]
        public async Task<ActionResult<User>>GetUser()
        {
            var result = await _queryBus.Send<GetUserQuery, User>(new GetUserQuery());

            if (result == null)
                return NotFound();

            return Ok(result);
        }

        [HttpGet("sort")]
        public async Task<ActionResult<List<Product>>> Sort(SortOption sortOption)
        {
            var query = _mapper.Map<GetSortedProductsQuery>(sortOption);

            var result = await _queryBus.Send<GetSortedProductsQuery, List<Product>>(query);

            if (result == null)
            {
                return NotFound(); 
            }

            return Ok(result);
        }

        [HttpPost("trolleyTotal")]
        public async Task<ActionResult<decimal>> TrolleyTotal([FromBody] TrolleyTotalRequest request)
        {
            var query = _mapper.Map<GetTrolleyTotalQuery>(request);

            var result = await _queryBus.Send<GetTrolleyTotalQuery, decimal>(query);

            return Ok(result);
        }
    }
}