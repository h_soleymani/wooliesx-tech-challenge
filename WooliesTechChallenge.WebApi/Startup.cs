﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WooliesTechChallenge.ApiAccessor;
using WooliesTechChallenge.ApiAccessor.Interface;
using WooliesTechChallenge.Core.Queries;
using WooliesTechChallenge.Core.QueryHandlers;
using WooliesTechChallenge.Domain.Entities;
using WooliesTechChallenge.MediatR.Queries;

namespace WooliesTechChallenge.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddHttpClient<WooliesResourceClient>("WooliesX", c =>
            {
                c.BaseAddress = new Uri(Configuration["WolliesResource:ApiUrl:Base"]);
            });

            // Add access to generic IConfigurationRoot
            services.AddSingleton(Configuration);

            ConfigDependencyInjection(services);

            // Adding MediatR
            services.AddMediatR(typeof(Startup));

            services.AddAutoMapper(typeof(Startup));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }

        private void ConfigDependencyInjection(IServiceCollection services)
        {
            services.AddScoped<IWooliesResourceApiAccessor, WooliesResourceApiAccessor>();

            services.AddScoped<IQueryBus, QueryBus>();

            services.AddScoped<IRequestHandler<GetUserQuery, User>, UserQueryHandler>();
            services.AddScoped<IRequestHandler<GetSortedProductsQuery, List<Product>>, ProductsQueryHandler>();
            services.AddScoped<IRequestHandler<GetTrolleyTotalQuery, decimal>, TrolleyQueryHandler>();
        }
    }
}
