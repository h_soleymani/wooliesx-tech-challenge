﻿using System.Runtime.Serialization;

namespace WooliesTechChallenge.Domain.Constants
{
    public enum SortOption
    {
        [EnumMember(Value = "Low")]
        Low,
        [EnumMember(Value = "High")]
        High,
        [EnumMember(Value = "Ascending")]
        Ascending,
        [EnumMember(Value = "Descending")]
        Descending,
        [EnumMember(Value = "Recommended")]
        Recommended
    }
}
