﻿namespace WooliesTechChallenge.Domain.Entities
{
    public class ProductPopularity
    {
        public string Name { get; set; }

        public decimal OrderedQuantity { get; set; }

        public ProductPopularity(string name, decimal orderedQuantity)
        {
            this.Name = name;

            this.OrderedQuantity = orderedQuantity;
        }
    }
}
