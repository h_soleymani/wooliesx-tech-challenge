﻿using System.Collections.Generic;

namespace WooliesTechChallenge.Domain.Entities
{
    public class Special
    {
        public List<Quantity> Quantities { get; set; }

        public decimal Total { get; set; }
    }
}