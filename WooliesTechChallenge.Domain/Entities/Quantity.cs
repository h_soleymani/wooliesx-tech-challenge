﻿using Newtonsoft.Json;

namespace WooliesTechChallenge.Domain.Entities
{
    public class Quantity
    {
        public string Name { get; set; }

        [JsonProperty("quantity")]
        public int Value { get; set; }
    }
}