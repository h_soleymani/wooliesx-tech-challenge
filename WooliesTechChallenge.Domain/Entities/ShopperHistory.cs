﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WooliesTechChallenge.Domain.Entities
{
    public class ShopperHistory
    {
        public int CustomerId { get; set; }

        public IEnumerable<Product> Products { get; set; }
    }
}
