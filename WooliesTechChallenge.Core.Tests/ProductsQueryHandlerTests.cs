using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using WooliesTechChallenge.ApiAccessor.Interface;
using WooliesTechChallenge.Core.Queries;
using WooliesTechChallenge.Core.QueryHandlers;
using WooliesTechChallenge.Domain.Entities;
using WooliesTechChallenge.MediatR.Queries;
using Xunit;

namespace WooliesTechChallenge.Core.Tests
{
    public class ProductsQueryHandlerTests
    {
        private Mock<IWooliesResourceApiAccessor> wooliesResourceApiAccessorMock;

        [Fact]
        public async void products_query_handler_sort_option_equal_low_returns_products_low_to_high_price()
        {
            //Arrange
            var handler = GetProductsQueryHandler();

            var query = new GetSortedProductsQuery() {SortOption = Domain.Constants.SortOption.Low };
            
            //Act
            var result = await handler.Handle(query, new System.Threading.CancellationToken());

            //Assert
            Assert.NotNull(result);

            var productFirst = result.First();
            var productLast = result.Last();

            Assert.Equal("Product C", productFirst.Name);
            Assert.Equal(2.4m, productFirst.Price);
            Assert.Equal(2, productFirst.Quantity);
            Assert.Equal("Product E", productLast.Name);
            Assert.Equal(3333, productLast.Price);
            Assert.Equal(3, productLast.Quantity);
        }

        [Fact]
        public async void products_query_handler_sort_option_equal_high_returns_products_high_to_low_price()
        {
            //Arrange
            var handler = GetProductsQueryHandler();

            var query = new GetSortedProductsQuery() { SortOption = Domain.Constants.SortOption.High };
            
            //Act
            var result = await handler.Handle(query, new System.Threading.CancellationToken());

            //Assert
            Assert.NotNull(result);

            var productFirst = result.First();
            var productLast = result.Last();

            Assert.Equal("Product E", productFirst.Name);
            Assert.Equal(3333, productFirst.Price);
            Assert.Equal(3, productFirst.Quantity);
            Assert.Equal("Product C", productLast.Name);
            Assert.Equal(2.4m, productLast.Price);
            Assert.Equal(2, productLast.Quantity);
            Assert.Equal(5, result.Count);
        }

        [Fact]
        public async void products_query_handler_sort_option_equal_ascending_returns_products_sorted_by_name_a_z()
        {
            //Arrange
            var handler = GetProductsQueryHandler();

            var query = new GetSortedProductsQuery() { SortOption = Domain.Constants.SortOption.Ascending };

            //Act
            var result = await handler.Handle(query, new System.Threading.CancellationToken());

            //Assert
            Assert.NotNull(result);

            var productFirst = result.First();
            var productLast = result.Last();

            Assert.Equal(5, result.Count);
            Assert.Equal("Product A", productFirst.Name);
            Assert.Equal("Product E", productLast.Name);
        }

        [Fact]
        public async void products_query_handler_sort_option_equal_descending_returns_products_sorted_by_name_z_a()
        {
            //Arrange
            var handler = GetProductsQueryHandler();

            var query = new GetSortedProductsQuery() { SortOption = Domain.Constants.SortOption.Descending };
            
            //Act
            var result = await handler.Handle(query, new System.Threading.CancellationToken());

            //Assert
            Assert.NotNull(result);

            var productFirst = result.First();
            var productLast = result.Last();

            Assert.Equal(5, result.Count);
            Assert.Equal("Product E", productFirst.Name);
            Assert.Equal("Product A", productLast.Name);
        }


        [Fact]
        public async void products_query_handler_sort_option_equal_recommended_will_call_shopperHistory()
        {
            //Arrange
            var handler = GetProductsQueryHandler();

            var query = new GetSortedProductsQuery() { SortOption = Domain.Constants.SortOption.Recommended };

            //Act
            var result = await handler.Handle(query, new System.Threading.CancellationToken());

            //Assert
            wooliesResourceApiAccessorMock.Verify(m => m.GetShopperHistory(), Times.Once);
        }

        [Fact]
        public async void products_query_handler_sort_option_equal_recommended_returns_products_based_on_popularity()
        {
            //Arrange
            var handler = GetProductsQueryHandler(true);

            var query = new GetSortedProductsQuery() { SortOption = Domain.Constants.SortOption.Recommended };

            //Act
            var result = await handler.Handle(query, new System.Threading.CancellationToken());

            //Assert
            Assert.NotNull(result);

            var productFirst = result.First();
            var productLast = result.Last();

            Assert.Equal(5, result.Count);
            Assert.Equal("Product C", result.First().Name);
            Assert.Equal("Product B", result.Skip(1).First().Name);
            Assert.Equal("Product A", result.Skip(2).First().Name);
            Assert.Equal("Product D", result.Skip(3).First().Name);
            Assert.Equal("Product E", result.Skip(4).First().Name);
        }

        private ProductsQueryHandler GetProductsQueryHandler(bool callingShopperHistory = false)
        {
            wooliesResourceApiAccessorMock = new Mock<IWooliesResourceApiAccessor>();

            wooliesResourceApiAccessorMock.Setup(m => m.GetProducts())
                .Returns(async () => Products());

            if (callingShopperHistory)
            {
                wooliesResourceApiAccessorMock.Setup(m => m.GetShopperHistory())
                    .Returns(async () => CustomerShopperHistory());
            }

            return new ProductsQueryHandler(wooliesResourceApiAccessorMock.Object);
        }

        private List<Product> Products()
        {
            return new List<Product>
            {
                new Product {Name = "Product A", Price = 6.2m, Quantity = 1},
                new Product {Name = "Product C", Price = 2.4m, Quantity = 2},
                new Product {Name = "Product E", Price = 3333, Quantity = 3},
                new Product {Name = "Product D", Price = 21, Quantity = 1},
                new Product {Name = "Product B", Price = 11, Quantity = 0}
            };
        }
        private List<Product> Products1()
        {
            return new List<Product>
            {
                new Product {Name = "Product C", Price = 2, Quantity = 3},
                new Product {Name = "Product B", Price = 5, Quantity = 1},
            };
        }
        private List<Product> Products2()
        {
            return new List<Product>
            {
                new Product {Name = "Product A", Price = 2, Quantity = 1},
                new Product {Name = "Product B", Price = 5, Quantity = 1},
            };
        }


        private List<ShopperHistory> CustomerShopperHistory()
        {
            return new List<ShopperHistory>()
            {

                new ShopperHistory()
                {
                    CustomerId = 1,
                    Products = Products1()
                },
                new ShopperHistory()
                {
                    CustomerId = 2,
                    Products = Products2()
                }
            };
        }
    }
}
