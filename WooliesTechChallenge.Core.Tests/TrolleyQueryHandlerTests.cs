using AutoMapper;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using WooliesTechChallenge.ApiAccessor.Interface;
using WooliesTechChallenge.Core.Queries;
using WooliesTechChallenge.Core.QueryHandlers;
using WooliesTechChallenge.Domain.Entities;
using WooliesTechChallenge.MediatR.Queries;
using Xunit;

namespace WooliesTechChallenge.Core.Tests
{
    public class TrolleyQueryHandlerTests
    {
        [Fact]
        public async void products_query_handler_sort_option_equal_recommended_will_call_get_trolley_total()
        {
            //Arrange
            var wooliesResourceApiAccessorMock = new Mock<IWooliesResourceApiAccessor>();
            var mapperMock = new Mock<IMapper>();
            var trolley = new Trolley();

            mapperMock.Setup(m => m.Map<Trolley>(It.IsAny<GetTrolleyTotalQuery>())).Returns(trolley);

            var handler = new TrolleyQueryHandler(wooliesResourceApiAccessorMock.Object, mapperMock.Object);
            

            var query = new GetTrolleyTotalQuery() {  };

            //Act
            var result = await handler.Handle(query, new System.Threading.CancellationToken());

            //Assert
            wooliesResourceApiAccessorMock.Verify(m => m.GetTrolleyTotal(trolley), Times.Once);
        }
    }
}
