﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WooliesTechChallenge.ApiAccessor.Interface;
using WooliesTechChallenge.Core.Queries;
using WooliesTechChallenge.Domain.Constants;
using WooliesTechChallenge.Domain.Entities;
using WooliesTechChallenge.MediatR.Queries;

namespace WooliesTechChallenge.Core.QueryHandlers
{
    public class ProductsQueryHandler : IQueryHandler<GetSortedProductsQuery, List<Product>>
    {
        private readonly IWooliesResourceApiAccessor _wooliesResourceApiAccessor;

        public ProductsQueryHandler(IWooliesResourceApiAccessor wooliesResourceApiAccessor)
        {
            _wooliesResourceApiAccessor = wooliesResourceApiAccessor ?? throw new ArgumentNullException(nameof(wooliesResourceApiAccessor));
        }

        public async Task<List<Product>> Handle(GetSortedProductsQuery request, CancellationToken cancellationToken)
        {
            var products = await _wooliesResourceApiAccessor.GetProducts();

            switch (request.SortOption)
            {
                case SortOption.Low:
                    return products.OrderBy(x => x.Price).ToList();

                case SortOption.High:
                    return products.OrderByDescending(x => x.Price).ToList();

                case SortOption.Ascending:
                    return products.OrderBy(x => x.Name).ToList();

                case SortOption.Descending:
                    return products.OrderByDescending(x => x.Name).ToList();

                case SortOption.Recommended:
                    var popularProduct = await GetProductPopularityByShoppingHistory();

                    return products
                        .OrderByDescending(p => popularProduct.SingleOrDefault(x => x.Name == p.Name)?.OrderedQuantity)
                        .ThenBy(p => p.Name)
                        .ToList();
                default:
                    throw new ArgumentOutOfRangeException(nameof(request.SortOption), request.SortOption, null);
            }
        }

        private async Task<List<ProductPopularity>> GetProductPopularityByShoppingHistory()
        {
            var shopperHistory = await _wooliesResourceApiAccessor.GetShopperHistory();

            return shopperHistory.SelectMany(x => x.Products)
                    .GroupBy(x => (x.Name))
                    .Select(x => new ProductPopularity(x.First().Name, x.Sum(y => y.Quantity)))
                    .ToList();
        }
    }
}
