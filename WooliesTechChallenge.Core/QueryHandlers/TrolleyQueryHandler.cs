﻿using AutoMapper;
using System;
using System.Threading;
using System.Threading.Tasks;
using WooliesTechChallenge.ApiAccessor.Interface;
using WooliesTechChallenge.Core.Queries;
using WooliesTechChallenge.Domain.Entities;
using WooliesTechChallenge.MediatR.Queries;

namespace WooliesTechChallenge.Core.QueryHandlers
{
    public class TrolleyQueryHandler : IQueryHandler<GetTrolleyTotalQuery, decimal>
    {
        private readonly IWooliesResourceApiAccessor _wooliesResourceApiAccessor;
        private readonly IMapper _mapper;

        public TrolleyQueryHandler(IWooliesResourceApiAccessor wooliesResourceApiAccessor, IMapper mapper)
        {
            _wooliesResourceApiAccessor = wooliesResourceApiAccessor;
            _mapper = mapper;
        }

        public async Task<decimal> Handle(GetTrolleyTotalQuery request, CancellationToken cancellationToken)
        {
            var trolleyTotalRequest = _mapper.Map<Trolley>(request);

            return await _wooliesResourceApiAccessor.GetTrolleyTotal(trolleyTotalRequest);
        }
    }
}
