﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using WooliesTechChallenge.Core.Queries;
using WooliesTechChallenge.Domain.Entities;
using WooliesTechChallenge.MediatR.Queries;

namespace WooliesTechChallenge.Core.QueryHandlers
{
    public class UserQueryHandler : IQueryHandler<GetUserQuery, User>
    {
        private readonly IConfiguration Config;
        private readonly ILogger _logger;

        public UserQueryHandler(IConfiguration config, ILogger<UserQueryHandler> logger)
        {
            Config = config ?? throw new ArgumentNullException(nameof(config));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public Task<User> Handle(GetUserQuery request, CancellationToken cancellationToken)
        {
            if (string.IsNullOrEmpty(Config["WolliesResource:Identifier:Name"]) || string.IsNullOrEmpty(Config["WolliesResource:Identifier:Token"]))
            {
                _logger.LogError("Missing Identifiers in appsettings");

                return null;
            }

            var user = new User() { Name = Config["WolliesResource:Identifier:Name"], Token = Config["WolliesResource:Identifier:Token"] };

            return Task.FromResult(user);
        }
    }
}
