﻿using WooliesTechChallenge.Domain.Entities;
using WooliesTechChallenge.MediatR.Queries;

namespace WooliesTechChallenge.Core.Queries
{
    public class GetUserQuery : IQuery<User>
    {
    }
}
