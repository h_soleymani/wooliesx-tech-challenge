﻿using System.Collections.Generic;
using WooliesTechChallenge.Domain.Constants;
using WooliesTechChallenge.Domain.Entities;
using WooliesTechChallenge.MediatR.Queries;

namespace WooliesTechChallenge.Core.Queries
{
    public class GetSortedProductsQuery : IQuery<List<Product>>
    {
        public SortOption SortOption { get; set; }
    }
}
