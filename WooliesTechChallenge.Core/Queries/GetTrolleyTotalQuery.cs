﻿using System.Collections.Generic;
using WooliesTechChallenge.Domain.Entities;
using WooliesTechChallenge.MediatR.Queries;

namespace WooliesTechChallenge.Core.Queries
{
    public class GetTrolleyTotalQuery : IQuery<decimal>
    {
        public IEnumerable<Product> Products { get; set; }

        public IEnumerable<Special> Specials { get; set; }

        public IEnumerable<Quantity> Quantities { get; set; }
    }
}
